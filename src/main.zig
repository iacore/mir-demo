const std = @import("std");
const m = @import("mir.zig");

pub fn main() !void {
    const ctx = m.MIR_init() orelse return error.MIRError;

    const mod = m.MIR_new_module(ctx, "name");

    var f_vars = [_]m.MIR_var_t{
        .{ .type = m.MIR_T_I64, .name = "a" },
        .{ .type = m.MIR_T_I64, .name = "b" },
    };
    const f = ctx.new_func("add", 1, &.{m.MIR_T_I64}, 2, &f_vars);
    const fid = f.u.func;

    const ret = ctx.new_func_reg(fid, m.MIR_T_I64, "ret");
    const a = ctx.reg(f_vars[0].name, fid);
    const b = ctx.reg(f_vars[1].name, fid);

    ctx.append(f, ctx.insn(m.MIR_ADD, 3, &.{ ctx.op_reg(ret), ctx.op_reg(a), ctx.op_reg(b) }));

    ctx.append(f, ctx.insn(m.MIR_RET, 1, &.{ctx.op_reg(ret)}));
    m.MIR_finish_func(ctx);

    m.MIR_finish_module(ctx);

    m.MIR_load_module(ctx, mod);

    m.MIR_output(ctx, m.stderr);

    m.MIR_gen_init(ctx, 1);
    m.MIR_link(ctx, m.MIR_set_gen_interface, null);
    const f_call = @as(*fn (i64, i64) callconv(.C) i64, @ptrCast(m.MIR_gen(ctx, 0, f)));
    m.MIR_gen_finish(ctx);

    const ans = f_call(2, 3);
    std.log.info("{}", .{ans});
}
