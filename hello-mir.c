#include "mir.h"

int main() {
  MIR_context_t ctx = MIR_init();
  MIR_module_t mir_module = MIR_new_module(ctx, "name");
  MIR_type_t i = MIR_T_I64;
  MIR_item_t func = MIR_new_func(ctx, "hello", 1, &i, 2,
                                 MIR_T_I64, "a",
                                 MIR_T_I64, "b"
  );
  MIR_func_t fid = func->u.func;
  MIR_reg_t ret = MIR_new_func_reg(ctx, fid, MIR_T_I64, "$ret");
  MIR_reg_t a = MIR_reg(ctx, "a", fid);
  MIR_reg_t b = MIR_reg(ctx, "b", fid);
  MIR_append_insn(ctx, func,
                  MIR_new_insn(ctx, MIR_ADD, MIR_new_reg_op(ctx, ret), MIR_new_reg_op(ctx, a), MIR_new_reg_op(ctx, b)));
  MIR_append_insn(ctx, func,
                  MIR_new_ret_insn(ctx, 1, MIR_new_reg_op(ctx, ret)));
  MIR_finish_func(ctx);
  MIR_finish_module(ctx);

  return 0;
}
